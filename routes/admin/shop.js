var express = require ('express');
var router = express.Router ();
var productService = require ('../../services/ShopService');
var upload = require ('../../middlewares/uploaderMiddleware');

router.get('/', function(req, res, next) {
  var products = productService.getProducts();

  var data = {
    products: products
  }

  res.render('admin/shop', data);
});

router.get('/create', function(req, res, next){

  res.render('admin/shop/create');

});

router.post('/create', upload.single('image'), function(req, res, next){
  var products = productService.saveProducts();

  var newId = products.length +1;

  var newProduct = {};
  newProduct.id = newId;
  newProduct.title = req.body.title;
  newProduct.description = req.body.description;
  newProduct.image = req.file.filename;
  newProduct.body = req.body.productBody;

  productService.saveProducts(newProduct);

  res.redirect('/admin/shop')

})



module.exports = router;

