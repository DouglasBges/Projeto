var fs = require ('fs');

var projectFilePath = 'db/projects.json';

var loadFileProjects = function(){
  var fileData = fs.readFileSync(projectFilePath,'utf8');
  var projects = JSON.parse(fileData);

  return projects;
}

var saveFileProjects = function(projects) {
  var data =JSON.stringify(projects);
  fs.writeFileSync(projectFilePath, data, 'utf8');
}

var getProjects = function() {
var projects = loadFileProjects(); 
  return projects;
}
var saveProjects = function(newProject) {
  var projects = loadFileProjects();
  projects.push(newProject);
  saveFileProjects(projects);
}
module.exports = {
  getProjects: getProjects,
  saveProjects: saveProjects
}