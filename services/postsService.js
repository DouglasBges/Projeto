var fs = require ('fs');

var postFilePath = 'db/posts.json';

var loadFilePosts = function(){
  var fileData = fs.readFileSync(postFilePath, 'utf8'); // ler arquivos do banco 
 var posts = JSON.parse(fileData);

  return posts;
}

var saveFilePosts = function(posts) {
  var data = JSON.stringify(posts);
  fs.writeFileSync(postFilePath, data, 'utf8');
}

var getPosts = function() {
  var posts = loadFilePosts();
  return posts;
}

var savePost = function(newPost) {
  var posts = loadFilePosts();
  posts.push(newPost);
  saveFilePosts(posts);
}

module.exports = {
  getPosts: getPosts, 
  savePost: savePost  // controle de acesso a dados em outras partes do sistema.
}