var fs = require ('fs');

var productFilePath = 'db/shop.json';

var loadFileProducts = function(){
  var fileData = fs.readFileSync(productFilePath,'utf8');
  var products = JSON.parse(fileData);

  return products;
}

var saveFileProducts = function(products) {
  var data =JSON.stringify(products);
  fs.writeFileSync(productFilePath, data, 'utf8');
}

var getProducts = function() {
var products = loadFileProducts(); 
  return products;
}
var saveProducts = function(newProduct) {
  var products = loadFileProducts();
  products.push(newProduct);
  saveFileProducts(products);
}
module.exports = {
  getProducts: getProducts,
  saveProducts: saveProducts
}