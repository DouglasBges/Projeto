

var getExecutive = function () {
  var executives = [
    {
      id: 1,
      nome: 'Douglas Borges Barbosa',
      cargo: 'CEO',
      imagem: 'douglas.jpg',
      linkedin: 'https://www.linkedin.com/in/douglas-borges-941a8217b/',
    },
    {
      id: 2,
      nome: 'Seu madruga',
      cargo: 'Produtor',
      imagem: 'jose.jpg',
      linkedin: 'https://www.linkedin.com/in/me/',
    }, 
    {
      id: 3,
      nome: 'Chaves',
      cargo: 'Full-Stack Developer',
      imagem: 'chaves.jpg',
      linkedin: 'https://www.linkedin.com/in/me/',
    }, 
    {
      id: 4,
      nome: 'Kiko',
      cargo: 'Analista de sistemas',
      imagem: 'kiko.jpg',
      linkedin: 'https://www.linkedin.com/in/me/',
    }, 
    {
      id: 5,
      nome: 'Dona Florinda',
      cargo: 'Gerente',
      imagem: 'florinda.jpg',
      linkedin: 'https://www.linkedin.com/in/me/',
    } 
  ];

    return executives;
}

module.exports = {
  getExecutive: getExecutive
}